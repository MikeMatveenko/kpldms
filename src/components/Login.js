import React from 'react';
import '../assets/css/Login.css';
import logo from '../assets/images/login/form-logo.svg';
import back from '../assets/images/login/login-back.png';

class Login extends React.Component {
  render() {
    return (
      <div className="login">
        <div className="form">
          <img className="logo" src={logo} alt="" />

          <div className="input-wrapper username">
            <label>Логин *</label>
            <input placeholder="Введите логин"></input>
          </div>

          <div className="input-wrapper password">
            <label>Пароль *</label>
            <input placeholder="Введите пароль"></input>
          </div>

          <button className="sub-white sign-in">войти</button>

          <button className="sub-white forgot">забыли пароль</button>
        </div>

        <img src={back} />
      </div>
    );
  }
}

export default Login;