import '../assets/css/Activity.css';
import React from 'react';
import { useHistory } from 'react-router-dom';
import img1 from '../assets/images/activity/1.png';
import img2 from '../assets/images/activity/2.png';
import FindBox from './FindBox';

function Activity () {
  let history = useHistory();

  function handleClick() {
    history.push("/activity/1");
  }

  return (
    <div className="activity">
      <div className="page-header">
        <p>Мероприятия</p>
      </div>

      <div className="list">
        <div className="card" onClick={handleClick}>
          <div className="img">
            <img src={img1} alt="" />
          </div>

          <div className="info">
            <p className="name">Первое выездное заседание Клуба «Первых лиц»</p>

            <p className="date">01/09/2020</p>

            <p className="description">3 сентября 2020 года состоится первое выездное заседание Клуба «Первых Лиц» на площадке ООО «Морозко». Философия каждого выездного заседания…</p>

            <button className="more sub-without">больше</button>
          </div>
        </div>
        <div className="card">
          <div className="img">
            <img src={img2} alt="" />
          </div>

          <div className="info">
            <p className="name">Первое заседание Клуба «Первых лиц»</p>

            <p className="date">01/08/2020</p>

            <p className="description">5 декабря 2019 года состоялось первое организационное заседание Клуба «Первых лиц». На мероприятии был утвержден Устав Клуба, избран Президиум Клуба, утвержден план работ ...</p>

            <button className="more sub-without">больше</button>
          </div>
        </div>
      </div>

      <FindBox />
    </div>
  );
}

export default Activity;