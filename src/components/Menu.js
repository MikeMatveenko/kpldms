import React from 'react';
import { NavLink } from "react-router-dom";
import '../assets/css/Menu.css';
import logo from '../assets/images/menu/logo.svg';
import accountPhoto from '../assets/images/menu/account/avatar.png';

class Menu extends React.Component {
  constructor (props) {
    super(props)

    this.accountMenuRef = React.createRef()
    this.burger = React.createRef()

    this.accMenu = this.accMenu.bind(this)
    this.linkClick = this.linkClick.bind(this)
  }

  render() {
    return (
      <div className="menu">
        <input id="menu__toggle" type="checkbox" ref={this.burger} />
        <label className="menu__btn" htmlFor="menu__toggle" />

        <NavLink to="/" className="logo"><img src={logo} alt=""></img></NavLink>

        <div className="wrapper">
          <div className="main">
            <NavLink onClick={this.linkClick} exact to="/">о клубе</NavLink>
            <NavLink onClick={this.linkClick} exact to="/team-club">команда клуба</NavLink>
            <NavLink onClick={this.linkClick} to="/business-ideas">бизнес-идеи</NavLink>
            <NavLink onClick={this.linkClick} to="/activity">мероприятия</NavLink>
            <NavLink onClick={this.linkClick} exact to="/recomendations">рекомендации</NavLink>
            <NavLink onClick={this.linkClick} to="/young-first">young first</NavLink>
            <NavLink onClick={this.linkClick} exact to="/hobby">хобби</NavLink>
            <NavLink onClick={this.linkClick} exact to="/contacts">контакты</NavLink>
          </div>

          <div className="account" onClick={this.accMenu}>
            <img src={accountPhoto} alt="" />

            <p>Йоханнес Т.</p>

            <div className="account-menu" ref={this.accountMenuRef}>
              <NavLink onClick={this.linkClick} exact to="/profile">Профиль</NavLink>

              <NavLink onClick={this.linkClick} exact to="/login">Выйти</NavLink>
            </div>
          </div>
        </div>
      </div>
    );
  }

  accMenu () {
    this.accountMenuRef.current.classList.toggle('active')
  }

  linkClick () {
    this.burger.current.click()
  }
}

export default Menu;