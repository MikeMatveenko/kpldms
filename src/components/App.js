import '../assets/css/App.css';
import React from 'react';
import Menu from './Menu';
import Footer from './Footer';
import Main from './Main';
import Login from './Login';
import NoMatch from './NoMatch';
import TeamClub from './TeamClub';
import BusinessIdeas from './BusinessIdeas';
import BusinessIdeaFull from './BusinessIdeaFull';
import ReviewFull from './ReviewFull';
import Activity from './Activity';
import ActivityFull from './ActivityFull';
import Recomendations from './Recomendations';
import YoungFirst from './YoungFirst';
import YoungFirstFull from './YoungFirstFull';
import Hobby from './Hobby';
import Contacts from './Contacts';
import Profile from './Profile';
import FindBox from './FindBox';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/login" component={Login}>

          </Route>
          <Route exact path="/">
            <Menu />

            <Main />

            <Footer />
          </Route>
          <Route exact path="/team-club">
            <Menu />

            <TeamClub />

            <Footer />
          </Route>
          <Route exact path="/business-ideas">
            <Menu />

            <BusinessIdeas />

            <Footer />
          </Route>
          <Route exact path="/business-ideas/ideas/:id">
            <Menu />

            <BusinessIdeaFull />

            <Footer />
          </Route>
          <Route exact path="/business-ideas/reviews/:id">
            <Menu />

            <ReviewFull />

            <Footer />
          </Route>
          <Route exact path="/activity">
            <Menu />

            <Activity />

            <Footer />
          </Route>
          <Route exact path="/activity/:id">
            <Menu />

            <ActivityFull />

            <Footer />
          </Route>
          <Route exact path="/recomendations">
            <Menu />

            <Recomendations />

            <Footer />
          </Route>
          <Route exact path="/young-first">
            <Menu />

            <YoungFirst />

            <Footer />
          </Route>
          <Route exact path="/young-first/:id">
            <Menu />

            <YoungFirstFull />

            <Footer />
          </Route>
          <Route exact path="/hobby">
            <Menu />

            <Hobby />

            <Footer />
          </Route>
          <Route exact path="/contacts">
            <Menu />

            <Contacts />

            <Footer />
          </Route>
          <Route exact path="/profile">
            <Menu />

            <Profile />

            <Footer />
          </Route>
          <Route>
            <Menu />

            <NoMatch />

            <Footer />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default App;