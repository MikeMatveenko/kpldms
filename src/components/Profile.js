import '../assets/css/Profile.css';
import React from 'react';
import {
  PageList,
  PageLink
} from './PageList';
import Select from './Select';
import FindBox from './FindBox';
import img from '../assets/images/menu/account/avatar.png';
import send from '../assets/images/business-ideas/ideas/send.svg';
import burger from '../assets/images/burger.svg';
import x from '../assets/images/business-ideas/ideas/x.svg';
import instagram from '../assets/images/social-icons/instagram.svg';
import facebook from '../assets/images/social-icons/facebook.svg';
import twitter from '../assets/images/social-icons/twitter.svg';

class Profile extends React.Component {
  render() {
    return (
      <div className="profile">
        <div className="page-header">
          <p>профиль</p>
        </div>

        <div className="container">
          <PageList>
            <PageLink active href="#common" label="основная информация" />
            <PageLink href="#password" label="изменить пароль" />
            <PageLink href="#web-links" label="веб-ссылки" />
            <PageLink href="#contacts" label="личные контакты" />
            <PageLink href="#company-contacts" label="контакты компании" />
          </PageList>

          <div className="sections">
            <div id="common">
              <div className="fields">
                <p className="header">основная информация</p>

                <div className="img">
                  <img src={img} />

                  <div className="upload">
                    <img src={send} alt="" />

                    <p>Загрузить фото</p>
                  </div>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Фамилия"></input>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Имя"></input>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Логин"></input>
                </div>

                <button className="send sub-green">Сохранить</button>
              </div>
            </div>

            <div id="password">
              <div className="fields">
                <p className="header">изменить пароль</p>

                <div className="input-wrapper">
                  <label>Текущий пароль*</label>

                  <input placeholder="Введите текущий пароль"></input>
                </div>

                <div className="input-wrapper">
                  <label>Новый пароль*</label>

                  <input placeholder="Введите новый пароль"></input>
                </div>

                <div className="input-wrapper">
                  <label>Подтверждение пароля*</label>

                  <input placeholder="Подтвердите пароль"></input>
                </div>

                <button className="send sub-green">Сохранить</button>
              </div>
            </div>

            <div id="web-links">
              <div className="fields">
                <p className="header">веб-ссылки</p>

                <div className="input-wrapper">
                  <input placeholder="Введите заголовок ссылки"></input>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Введите URL-адрес"></input>
                </div>

                <button className="send sub-green">Добавить</button>
              </div>

              <div className="added">
                <div className="link">
                  <div>
                    <img src={burger} alt="" />

                    <p className="name">Сайт Йоханесса Толай</p>
                  </div>

                  <div>
                    <a href="https://j.tholey.ru/" className="url">https://j.tholey.ru/</a>

                    <img src={x} alt="" />
                  </div>
                </div>
              </div>
            </div>

            <div id="contacts">
              <div className="fields">
                <p className="header">личные контакты</p>

                <div className="input-wrapper">
                  <input placeholder="Страна"></input>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Город"></input>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Телефон"></input>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Skype"></input>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Личный сайт"></input>
                </div>

                <button className="send sub-green">Сохранить</button>
              </div>
            </div>

            <div id="company-contacts">
              <div className="fields">
                <p className="header">Контакты компании</p>

                <div className="input-wrapper">
                  <input placeholder="Название"></input>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Адрес"></input>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Сайт"></input>
                </div>

                <div className="input-wrapper">
                  <input placeholder="Телефон"></input>
                </div>

                <button className="send sub-green">Сохранить</button>
              </div>

              <div className="company-links">
                <p className="header">Ссылки на страницы компании в социальных сетях</p>

                <div className="fields">
                  <div className="input-wrapper">
                    <Select />
                  </div>

                  <div className="input-wrapper">
                    <input placeholder="Введите URL-адрес"></input>
                  </div>

                  <button className="send sub-green">Добавить</button>
                </div>
              </div>

              <div className="added">
                <div className="link">
                  <div>
                    <img src={burger} alt="" />

                    <img src={facebook} alt="" />

                    <p className="name">Instagram</p>
                  </div>

                  <div>
                    <a href="https://www.instagram.com/auchan" className="url">https://www.instagram.com/auchan</a>

                    <img src={x} alt="" />
                  </div>
                </div>

                <div className="link">
                  <div>
                    <img src={burger} alt="" />

                    <img src={instagram} alt="" />

                    <p className="name">Facebook</p>
                  </div>

                  <div>
                    <a href="https://www.instagram.com/auchan" className="url">https://www.instagram.com/auchan</a>

                    <img src={x} alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <FindBox />
      </div>
    );
  }
}

export default Profile;