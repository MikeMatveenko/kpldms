import '../assets/css/YoungFirst.css';
import React from 'react';
import AccountCard from './AccountCard';
import { useHistory } from 'react-router-dom';
import FindBox from './FindBox';
import newm from '../assets/images/young-first/new.png';

import img1 from '../assets/images/young-first/members/1.png';
import img2 from '../assets/images/young-first/members/2.png';
import img3 from '../assets/images/young-first/members/3.png';
import img4 from '../assets/images/young-first/members/4.png';
import img5 from '../assets/images/young-first/members/5.png';
import img6 from '../assets/images/young-first/members/6.png';
import img7 from '../assets/images/young-first/members/7.png';
import img8 from '../assets/images/young-first/members/8.png';
import img9 from '../assets/images/young-first/members/9.png';

import new1 from '../assets/images/young-first/news/1.png';
import new2 from '../assets/images/young-first/news/2.png';
import new3 from '../assets/images/young-first/news/3.png';
import new4 from '../assets/images/young-first/news/4.png';

function YoungFirst () {
  let history = useHistory();

  function handleClick() {
    history.push("/young-first/1");
  }

  return (
    <div className="young-first">
      <div className="page-header">
        <p>young first</p>
      </div>

      <div className="preview">
        <div className="info">
          <p className="name">Молодежная команда Клуба «Первых лиц»</p>

          <div className="img">
            <img src={newm} alt="" />
          </div>

          <p className="description">В немецком Клубе «МММ» существует практика «YOUNG ENTREPRENEURS IN THE MMM-CLUB» для сплочения и общения детей членов Клуба «Первых Лиц». 3 сентября 2020 года состоится первое организационное заседание «Young First» Клуба «Первых лиц»</p>
        </div>
      </div>

      <div className="container">
        <div className="characters">
          <a href="#А">А</a>
          <a href="#Б">Б</a>
          <a href="#В">В</a>
          <a href="#Д">Д</a>
          <a href="#Е">Е</a>
          <a href="#Ё">Ё</a>
          <a href="#Ж">Ж</a>
          <a href="#З">З</a>
          <a href="#И">И</a>
          <a href="#К">К</a>
          <a href="#Л">Л</a>
          <a href="#М">М</a>
          <a href="#Н">Н</a>
          <a href="#О">О</a>
          <a href="#П">П</a>
          <a href="#Р">Р</a>
          <a href="#С">С</a>
          <a href="#Т">Т</a>
          <a href="#У">У</a>
          <a href="#Ф">Ф</a>
          <a href="#Х">Х</a>
          <a href="#Ц">Ц</a>
          <a href="#Ч">Ч</a>
          <a href="#Щ">Щ</a>
          <a href="#Ш">Ш</a>
          <a href="#Ъ">Ъ</a>
          <a href="#Ы">Ы</a>
          <a href="#Ь">Ь</a>
          <a href="#Э">Э</a>
          <a href="#Ю">Ю</a>
          <a href="#Я">Я</a>
        </div>

        <div className="members">
          <AccountCard
            id="С"
            president="Президент Клуба «Young First»"
            img={img1}
            name="Семенов Антон"
            job="АО «Белая Дача»"
            position="Генеральный директор"
            social={[
              {
                type: 'instagram',
                href: "https://instagram.com"
              },
              {
                type: 'facebook',
                href: 'https://facebook.com'
              },
              {
                type: 'twitter',
                href: 'https://twitter.com'
              }
            ]} />
          <AccountCard
            img={img2}
            name="Гусев Дмитрий"
            job="ГК «Славянка»"
            position="Председатель Совета Директоров"
            social={[
              {
                type: 'instagram',
                href: "https://instagram.com"
              },
              {
                type: 'facebook',
                href: 'https://facebook.com'
              },
              {
                type: 'twitter',
                href: 'https://twitter.com'
              }
            ]} />
          <AccountCard
            img={img3}
            name="Деменов Борис"
            job="ГК «Грейн Холдинг»"
            position="Руководитель отдела продаж"
            social={[
              {
                type: 'instagram',
                href: "https://instagram.com"
              },
              {
                type: 'facebook',
                href: 'https://facebook.com'
              },
              {
                type: 'twitter',
                href: 'https://twitter.com'
              }
            ]} />
          <AccountCard
            img={img4}
            name="Добросоцкая Дарья"
            position="Студентка МГИМО МИД РФ"
            social={[
              {
                type: 'instagram',
                href: "https://instagram.com"
              },
              {
                type: 'facebook',
                href: 'https://facebook.com'
              },
              {
                type: 'twitter',
                href: 'https://twitter.com'
              }
            ]} />
          <AccountCard
            img={img5}
            name="Добросоцкий Иван"
            job="Клуб «Первых лиц»"
            position="Координатор"
            social={[
              {
                type: 'instagram',
                href: "https://instagram.com"
              },
              {
                type: 'facebook',
                href: 'https://facebook.com'
              },
              {
                type: 'twitter',
                href: 'https://twitter.com'
              }
            ]} />
          <AccountCard
            img={img6}
            name="Дюрр Екатерина"
            social={[
              {
                type: 'instagram',
                href: "https://instagram.com"
              },
              {
                type: 'facebook',
                href: 'https://facebook.com'
              },
              {
                type: 'twitter',
                href: 'https://twitter.com'
              }
            ]} />
          <AccountCard
            img={img7}
            name="Левин Иван"
            social={[
              {
                type: 'instagram',
                href: "https://instagram.com"
              },
              {
                type: 'facebook',
                href: 'https://facebook.com'
              },
              {
                type: 'twitter',
                href: 'https://twitter.com'
              }
            ]} />
          <AccountCard
            img={img8}
            name="Лола Тимофей"
            social={[
              {
                type: 'instagram',
                href: "https://instagram.com"
              },
              {
                type: 'facebook',
                href: 'https://facebook.com'
              },
              {
                type: 'twitter',
                href: 'https://twitter.com'
              }
            ]} />
          <AccountCard
            img={img9}
            name="Петрякова Наталья"
            social={[
              {
                type: 'instagram',
                href: "https://instagram.com"
              },
              {
                type: 'facebook',
                href: 'https://facebook.com'
              },
              {
                type: 'twitter',
                href: 'https://twitter.com'
              }
            ]} />
        </div>
      </div>

      <div className="page-header">
        <p>новости</p>
      </div>

      <div className="news">
        <div className="new" onClick={handleClick}>
          <div className="img">
            <img src={new1} alt="" />
          </div>

          <div className="info">
            <p className="name">Как создать бизнес с нуля</p>

            <p className="date">01/08/2020</p>

            <p className="description">Универсальные методики для любого вида предпринимательства от Андрея Казанцева.</p>

            <button className="more sub-without">подробнее</button>
          </div>
        </div>

        <div className="new" onClick={handleClick}>
          <div className="img">
            <img src={new2} alt="" />
          </div>

          <div className="info">
            <p className="name">В чем заключается важность PR для бизнеса?</p>

            <p className="date">01/08/2020</p>

            <p className="description">О важности пиара для бизнеса на примере GARTEL рассказывает...</p>

            <button className="more sub-without">подробнее</button>
          </div>
        </div>
        <div className="new" onClick={handleClick}>
          <div className="img">
            <img src={new3} alt="" />
          </div>

          <div className="info">
            <p className="name">Новые инструменты для бизнеса компании Oprosso</p>

            <p className="date">01/08/2020</p>

            <p className="description">Создан сервис для маркетинговых исследований и анализа кадро...</p>

            <button className="more sub-without">подробнее</button>
          </div>
        </div>
        <div className="new" onClick={handleClick}>
          <div className="img">
            <img src={new4} alt="" />
          </div>

          <div className="info">
            <p className="name">Открытие выставки «Всё начиналось в Кашире»</p>

            <p className="date">01/08/2020</p>

            <p className="description">Фонд собирает ненужную людям одежду и предметы быта. Плохие…</p>

            <button className="more sub-without">подробнее</button>
          </div>
        </div>
      </div>

      <FindBox />
    </div>
  );
}

export default YoungFirst;