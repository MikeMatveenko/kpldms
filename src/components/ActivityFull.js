import '../assets/css/ActivityFull.css';
import React from 'react';
import img from '../assets/images/activity/1.png';
import back from '../assets/images/back.svg';
import { useHistory } from 'react-router-dom';

function ActivityFull () {
  let history = useHistory();

  function handleClick() {
    history.push("/activity");
  }

  return (
    <div className="activity-full">
      <div className="back" onClick={handleClick}>
          <img src={back} alt="" />

          <p>Мероприятия</p>
      </div>

      <div className="all">
        <div className="info">
          <div className="img">
            <img src={img} alt="" />
          </div>

          <p className="name">Первое выездное заседание Клуба «Первых лиц»</p>

          <p>3 сентября 2020 года состоится первое выездное заседание Клуба «Первых Лиц» на площадке ООО «Морозко». Философия каждого выездного заседания имеет цель рассмотрения на практике самых сложных бизнес-ориентированных проблем управления и с помощью ведущих экспертов в этой области формирования у членов клуба понимания дальнейшего развития бизнеса. Член нашего Клуба Юрий Гусев любезно согласился поделиться с нами практиками внедрения искусственного интеллекта в его компании. Кроме того, мы рассмотрим самые эффективные кейсы применения искусственного интеллекта для повышения эффективности бизнеса как предприятий производителей, так и ритейла.</p>
        </div>

        <div className="form">
          <div className="input-wrapper">
            <label>ФИО *</label>

            <input placeholder="Введите ФИО"></input>
          </div>

          <div className="input-wrapper">
            <label>Компания *</label>

            <input placeholder="Введите компанию"></input>
          </div>

          <div className="input-wrapper">
            <label>Должность *</label>

            <input placeholder="Введите должность"></input>
          </div>

          <div className="input-wrapper">
            <label>Телефон *</label>

            <input placeholder="Введите телефон"></input>
          </div>

          <div className="input-wrapper">
            <label>E-mail *</label>

            <input placeholder="Введите e-mail"></input>
          </div>

          <div className="input-wrapper">
            <label>Дата *</label>

            <input placeholder="Введите дату"></input>
          </div>

          <div className="polit">
            <input type="checkbox" id="polit" />
            <label for="polit"></label>

            <label>Согласен с политикой обработки персональных данных</label>
          </div>

          <button className="send sub-white">Зарегистрироваться</button>
        </div>
      </div>
    </div>
  );
}

export default ActivityFull;