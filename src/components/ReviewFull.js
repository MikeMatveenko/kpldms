import React from 'react';
import '../assets/css/ReviewFull.css';
import back from '../assets/images/back.svg';
import img from '../assets/images/business-ideas/idea-1.png';
import photo from '../assets/images/menu/account/avatar.png';
import { useHistory } from 'react-router-dom';

function ReviewFull () {
  let history = useHistory();

  function handleClick() {
    history.push("/business-ideas");
  }

  return (
    <div className="review-full">
      <div className="all">
        <div class="info">
          <div className="back" onClick={handleClick}>
            <img src={back} alt="" />

            <p>Потенциальные партнеры</p>
          </div>

          <p className="header">морозко</p>

          <div className="rating">
            <p>3,7</p>

            <div className="stars">
              <div className="fill" />
              <div className="fill" />
              <div className="fill" />
              <div />
              <div />
            </div>

            <p>Отзывов: 12</p>
          </div>

          <p><a href="#">Официальный сайт</a></p>

          <img src={img} alt="" />

          <p className="text">
            Компания «МорозКом» — производитель замороженных полуфабрикатов с самым широким ассортиментом в России. На сегодняшний день мы выпускаем продукцию 13-ти категорий, в каждой из которых — десятки наименований. У нас широкий выбор вкуснейших сытных и десертных блинчиков, высококачественных пельменей премиум-класса, изысканной пиццы, вареников, готовых блюд, выпечки, замороженных овощных миксов. Наш ассортимент постоянно пополняется новой продукцией — компания «МорозКом» не стоит на месте, постоянно создавая новые вкуснейшие продукты, способные стать достойным дополнением вашего стола.
          </p>
        </div>
      </div>

      <div className="reviews">
        <p className="header">Отзывы 3</p>

        <div className="add">
          <div className="stars">
            <div />
            <div />
            <div />
            <div />
            <div />
          </div>

          <textarea placeholder="Введите отзыв"></textarea>

          <button className="send sub-green">отправить</button>
        </div>

        <div className="list">
          <div className="review">
            <img src={photo} alt="" />

            <div className="content">
              <p className="name">Семенов Антон</p>

              <div className="info">
                <div className="stars">
                  <div className="fill" />
                  <div className="fill" />
                  <div className="fill" />
                  <div />
                  <div />
                </div>

                <p className="date">20.08.2020 10:00</p>
              </div>

              <p>производство очень большое и мощное, с большим ассортиментом и неплохим оборудованием. Неплохая стартовая площадка для получение опыта молодым специалистом.</p>
            </div>
          </div>
          <div className="review">
            <img src={photo} alt="" />

            <div className="content">
              <p className="name">Семенов Антон</p>

              <div className="info">
                <div className="stars">
                  <div className="fill" />
                  <div className="fill" />
                  <div className="fill" />
                  <div />
                  <div />
                </div>

                <p className="date">20.08.2020 10:00</p>
              </div>

              <p>производство очень большое и мощное, с большим ассортиментом и неплохим оборудованием. Неплохая стартовая площадка для получение опыта молодым специалистом.</p>
            </div>
          </div>
          <div className="review">
            <img src={photo} alt="" />

            <div className="content">
              <p className="name">Семенов Антон</p>

              <div className="info">
                <div className="stars">
                  <div className="fill" />
                  <div className="fill" />
                  <div className="fill" />
                  <div />
                  <div />
                </div>

                <p className="date">20.08.2020 10:00</p>
              </div>

              <p>производство очень большое и мощное, с большим ассортиментом и неплохим оборудованием. Неплохая стартовая площадка для получение опыта молодым специалистом.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ReviewFull;