import React from 'react';
import '../assets/css/Review.css';
import { useHistory } from 'react-router-dom';

function Review (props) {
  let history = useHistory();

  function handleClick() {
    history.push("/business-ideas/reviews/1");
  }

  let stars = [];
  for (let i = 1; i <= 5; i++) {
    stars.push(<div className={props.rating[0] >= i ? "fill" : ""} />)
  }

  return (
    <div className="review" onClick={handleClick}>
      <div className="img">
        <img src={props.img} alt="" />
      </div>

      <div className="info">
        <p className="name">{props.name}</p>

        <div className="rating">
          <p>{props.rating}</p>

          <div className="stars">
            {stars}
          </div>

          <p>Отзывов: {props.reviews}</p>
        </div>

        <p className="description">{props.text}</p>
      </div>
    </div>
  );
}

export default Review;