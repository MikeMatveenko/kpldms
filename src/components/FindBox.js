import '../assets/css/FindBox.css';
import React from 'react';
import search from '../assets/images/main/search.svg';
import search_green from '../assets/images/find-box/find.svg';

class FindBox extends React.Component {
  render() {
    return (
      <div className={this.props.black ? "user-control black" : "user-control"}>
        <div className="search">
          <div className="back" />
          <img src={this.props.black ? search : search_green} alt="" />

          <input placeholder="Поиск"></input>
        </div>

        <div className="lang">
          <p className="ru active" />
          <p className="en" />
        </div>
      </div>
    );
  }
}

export default FindBox;