import React from 'react';
import '../assets/css/TeamClub.css';
import {
  PageList,
  PageLink
} from './PageList';
import AccountCard from './AccountCard';
import FindBox from './FindBox';

import imgm1 from '../assets/images/team-club/members/1.png';
import imgm2 from '../assets/images/team-club/members/2.png';
import imgm3 from '../assets/images/team-club/members/3.png';
import imgm4 from '../assets/images/team-club/members/4.png';
import imgm5 from '../assets/images/team-club/members/5.png';
import imgm6 from '../assets/images/team-club/members/6.png';
import imgm7 from '../assets/images/team-club/members/7.png';
import imgm8 from '../assets/images/team-club/members/8.png';
import imgm9 from '../assets/images/team-club/members/9.png';
import imgm10 from '../assets/images/team-club/members/10.png';
import imgm11 from '../assets/images/team-club/members/11.png';
import imgm12 from '../assets/images/team-club/members/12.png';
import imgm13 from '../assets/images/team-club/members/13.png';
import imgm14 from '../assets/images/team-club/members/14.png';

import imge1 from '../assets/images/team-club/experts/1.png';
import imge2 from '../assets/images/team-club/experts/2.png';
import imge3 from '../assets/images/team-club/experts/3.png';
import imge4 from '../assets/images/team-club/experts/4.png';
import imge5 from '../assets/images/team-club/experts/5.png';
import imge6 from '../assets/images/team-club/experts/6.png';
import imge7 from '../assets/images/team-club/experts/7.png';

import imgv1 from '../assets/images/team-club/viewers/1.png';
import imgv2 from '../assets/images/team-club/viewers/2.png';
import imgv3 from '../assets/images/team-club/viewers/3.png';
import imgv4 from '../assets/images/team-club/viewers/4.png';
import imgv5 from '../assets/images/team-club/viewers/5.png';
import imgv6 from '../assets/images/team-club/viewers/6.png';
import imgv7 from '../assets/images/team-club/viewers/7.png';
import imgv8 from '../assets/images/team-club/viewers/8.png';

class TeamClub extends React.Component {
  render() {
    return (
      <div className="team-club">
        <div className="page-header">
          <p>Команда клуба</p>
        </div>

        <div className="content">
          <PageList>
            <PageLink active href="#members" label="члены клуба" />
            <PageLink href="#experts" label="эксперты" />
            <PageLink href="#viewers" label="наблюдатели" />
          </PageList>

          <div className="sections">
            <div id="members">
              <div className="characters">
                <a href="#А1">А</a>
                <a href="#Б1">Б</a>
                <a href="#В1">В</a>
                <a href="#Д1">Д</a>
                <a href="#Е1">Е</a>
                <a href="#Ё1">Ё</a>
                <a href="#Ж1">Ж</a>
                <a href="#З1">З</a>
                <a href="#И1">И</a>
                <a href="#К1">К</a>
                <a href="#Л1">Л</a>
                <a href="#М1">М</a>
                <a href="#Н1">Н</a>
                <a href="#О1">О</a>
                <a href="#П1">П</a>
                <a href="#Р1">Р</a>
                <a href="#С1">С</a>
                <a href="#Т1">Т</a>
                <a href="#У1">У</a>
                <a href="#Ф1">Ф</a>
                <a href="#Х1">Х</a>
                <a href="#Ц1">Ц</a>
                <a href="#Ч1">Ч</a>
                <a href="#Щ1">Щ</a>
                <a href="#Ш1">Ш</a>
                <a href="#Ъ1">Ъ</a>
                <a href="#Ы1">Ы</a>
                <a href="#Ь1">Ь</a>
                <a href="#Э1">Э</a>
                <a href="#Ю1">Ю</a>
                <a href="#Я1">Я</a>
              </div>

              <div className="container">
                <p className="header">члены клуба</p>

                <AccountCard
                  president="Президент Клуба «Первых лиц»"
                  img={imgm1}
                  name="Толай Йоханнес"
                  job="«Ашан Ритейл Россия»"
                  position="Генеральный директор"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  id="Г1"
                  img={imgm2}
                  name="Гусев Дмитрий"
                  job="ГК «Славянка»"
                  position="Председатель Совета Директоров"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  img={imgm3}
                  name="Гусев Сергей"
                  job="ГК «Славянка»"
                  position="Основатель"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  img={imgm4}
                  name="Гусев Юрий"
                  job="ООО «Морозко»"
                  position="Собственник"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  id="Д1"
                  img={imgm5}
                  name="Деменов Алексей"
                  job="ГК «Грейн Холдинг»"
                  position="Генеральный директор"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  img={imgm6}
                  name="Добросоцкий Виктор"
                  job="Клуб «Первых лиц»"
                  position="Вице-президент"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  img={imgm7}
                  name="Дюрр Штефан"
                  job="УК «Эко-Нива АПК Холдинг»"
                  position="Генеральный директор"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  id="К1"
                  img={imgm8}
                  name="Касьяненко Сергей"
                  job="ГК «Орими»"
                  position="Председатель правления"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  id="Л1"
                  img={imgm9}
                  name="Лола Роман"
                  job="ООО РС «Айсберри»"
                  position="Генеральный директор"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  id="М1"
                  img={imgm10}
                  name="Матвеев Дмитрий"
                  job="ГК «Кабош»"
                  position="Президент"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  id="Н1"
                  img={imgm11}
                  name="Нестеров Николай"
                  job="ГК «НМЖК»"
                  position="Председатель Совета Директоров"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  id="С1"
                  img={imgm12}
                  name="Садовских Олег"
                  job="ЗАО «Смарт»"
                  position="Акционер"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  img={imgm13}
                  name="Семенов Антон"
                  job="АО «Белая Дача»"
                  position="Генеральный директор"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />

                <AccountCard
                  img={imgm14}
                  name="Семенов Виктор"
                  job="ГК «Белая Дача»"
                  position="Председатель наблюдательного совета"
                  social={[
                    {
                      type: 'instagram',
                      href: "https://instagram.com"
                    },
                    {
                      type: 'facebook',
                      href: 'https://facebook.com'
                    },
                    {
                      type: 'twitter',
                      href: 'https://twitter.com'
                    }
                  ]} />
              </div>
            </div>

            <div id="experts">
              <div className="characters">
                <a href="#А2">А</a>
                <a href="#Б2">Б</a>
                <a href="#В2">В</a>
                <a href="#Д2">Д</a>
                <a href="#Е2">Е</a>
                <a href="#Ё2">Ё</a>
                <a href="#Ж2">Ж</a>
                <a href="#З2">З</a>
                <a href="#И2">И</a>
                <a href="#К2">К</a>
                <a href="#Л2">Л</a>
                <a href="#М2">М</a>
                <a href="#Н2">Н</a>
                <a href="#О2">О</a>
                <a href="#П2">П</a>
                <a href="#Р2">Р</a>
                <a href="#С2">С</a>
                <a href="#Т2">Т</a>
                <a href="#У2">У</a>
                <a href="#Ф2">Ф</a>
                <a href="#Х2">Х</a>
                <a href="#Ц2">Ц</a>
                <a href="#Ч2">Ч</a>
                <a href="#Щ2">Щ</a>
                <a href="#Ш2">Ш</a>
                <a href="#Ъ2">Ъ</a>
                <a href="#Ы2">Ы</a>
                <a href="#Ь2">Ь</a>
                <a href="#Э2">Э</a>
                <a href="#Ю2">Ю</a>
                <a href="#Я2">Я</a>
              </div>

              <div className="container">
                <p className="header">эксперты</p>

                <AccountCard
                  id="З2"
                  img={imge1}
                  name="Занковский Анатолий"
                  position="Заведующий Лаборатории психологии труда, эргономики, инженерной и организационной психологии Института психологии РАН, научный руководитель центра «Управление Успехом»" />

                <AccountCard
                  id="К2"
                  img={imge2}
                  name="Казанников Денис"
                  position="Руководитель Проекта по поддержке экспорта АПК РЭЦ." />

                <AccountCard
                  img={imge3}
                  name="Кривоноженков Никита"
                  position="Основатель и собственник компании DM Solutions" />

                <AccountCard
                  id="М2"
                  img={imge4}
                  name="Морозов Александр"
                  position="Известный эксперт в области финансов, Директор департамента исследований и прогнозирования Центрального Банка России." />

                <AccountCard
                  img={imge5}
                  name="Морозов Игорь"
                  position="Сенатор Российской Федерации. Председатель Координационного Комитета по экономическому сотрудничеству со странами Африки (АФРОКОМ)." />

                <AccountCard
                  id="П2"
                  img={imge6}
                  name="Пристансков Дмитрий"
                  position="Вице-президент по GR «Норникель»" />

                <AccountCard
                  id="С2"
                  img={imge7}
                  name="Сирил Поль Бернард Пакари"
                  position="Генеральный директор CEVA LOGISTICS." />
              </div>
            </div>

            <div id="viewers">
              <div className="characters">
                <a href="#А3">А</a>
                <a href="#Б3">Б</a>
                <a href="#В3">В</a>
                <a href="#Д3">Д</a>
                <a href="#Е3">Е</a>
                <a href="#Ё3">Ё</a>
                <a href="#Ж3">Ж</a>
                <a href="#З3">З</a>
                <a href="#И3">И</a>
                <a href="#К3">К</a>
                <a href="#Л3">Л</a>
                <a href="#М3">М</a>
                <a href="#Н3">Н</a>
                <a href="#О3">О</a>
                <a href="#П3">П</a>
                <a href="#Р3">Р</a>
                <a href="#С3">С</a>
                <a href="#Т3">Т</a>
                <a href="#У3">У</a>
                <a href="#Ф3">Ф</a>
                <a href="#Х3">Х</a>
                <a href="#Ц3">Ц</a>
                <a href="#Ч3">Ч</a>
                <a href="#Щ3">Щ</a>
                <a href="#Ш3">Ш</a>
                <a href="#Ъ3">Ъ</a>
                <a href="#Ы3">Ы</a>
                <a href="#Ь3">Ь</a>
                <a href="#Э3">Э</a>
                <a href="#Ю3">Ю</a>
                <a href="#Я3">Я</a>
              </div>

              <div className="container">
                <p className="header">наблюдатели</p>

                <AccountCard
                  id="Б3"
                  img={imgv1}
                  name="Бабкин Николай"
                  position="Акционер KDV Group" />

                <AccountCard
                  id="В3"
                  img={imgv2}
                  name="Вальков Виталий"
                  position="Коммерческий директор «Пятерочка»" />

                <AccountCard
                  id="Г3"
                  img={imgv3}
                  name="Григорьев Алексей"
                  position="Вице-президент по внешней корпоративной политике Metro AG, глава представительства Metro AG в Москве." />

                <AccountCard
                  id="П3"
                  img={imgv4}
                  name="Подвальный Владимир"
                  position="Генеральный директор ООО «Великолукский Свиноводческий Комплекс»" />

                <AccountCard
                  id="С3"
                  img={imgv5}
                  name="Сологуб Денис"
                  position="Президент «Азбука Вкуса»" />

                <AccountCard
                  img={imgv6}
                  name="Сорокин Владимир"
                  position="Генеральный директор «ЛЕНТА»" />

                <AccountCard
                  id="Ф3"
                  img={imgv7}
                  name="Филипцев Владимир"
                  position="Заместитель генерального директора компании Simple" />

                <AccountCard
                  id="Щ3"
                  img={imgv8}
                  name="Щедрин Сергей"
                  position="Председатель правления АО «КБК Черёмушки»" />
              </div>
            </div>
          </div>
        </div>

        <FindBox />
      </div>
    );
  }
}

export default TeamClub;