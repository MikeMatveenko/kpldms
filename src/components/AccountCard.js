import React from 'react';
import '../assets/css/AccountCard.css';
import instagram from '../assets/images/social-icons/instagram.svg';
import facebook from '../assets/images/social-icons/facebook.svg';
import twitter from '../assets/images/social-icons/twitter.svg';

class AccountCard extends React.Component {
  render() {
    let socials = [];

    if (this.props.social)
      this.props.social.forEach(({href, type}) => {
        let icon = null;

        switch (type) {
          case 'instagram':
            icon = instagram;
            break;
          case 'facebook':
            icon = facebook;
            break;
          case 'twitter':
            icon = twitter;
            break;
        }

        socials.push(<a href={href}><img src={icon} alt="" /></a>)
      });

    return (
      <div id={this.props.id ? this.props.id : ""} className={this.props.president ? "president account-card" : "account-card"}>
        <div className="img">
          <img src={this.props.img} alt="" />
        </div>

        <div className="info">
          {this.props.president ? <p className="president">{this.props.president}</p> : ""}

          <p className="name">{this.props.name}</p>

          {this.props.job ? <p className="job">{this.props.job}</p> : ""}

          <p className="position">{this.props.position}</p>

          {this.props.social ? <div className="social">
            {socials}
          </div> : ""}
          
        </div>
      </div>
    );
  }
}

export default AccountCard;