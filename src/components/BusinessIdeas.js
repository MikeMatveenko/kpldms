import React from 'react';
import BusinessIdea from './BusinessIdea';
import Review from './Review';
import '../assets/css/BusinessIdeas.css';
import img1 from '../assets/images/business-ideas/idea-1.png';
import img2 from '../assets/images/business-ideas/idea-2.png';
import img3 from '../assets/images/business-ideas/idea-3.png';
import img4 from '../assets/images/business-ideas/idea-4.png';
import img5 from '../assets/images/business-ideas/idea-5.png';
import img6 from '../assets/images/business-ideas/idea-6.png';
import img7 from '../assets/images/business-ideas/idea-7.png';
import img8 from '../assets/images/business-ideas/idea-8.png';
import img9 from '../assets/images/business-ideas/idea-9.png';
import reviewImg1 from '../assets/images/main/links/3.png';
import reviewImg2 from '../assets/images/main/links/2.png';
import reviewImg3 from '../assets/images/main/links/8.png';
import FindBox from './FindBox';

class BusinessIdeas extends React.Component {
  render() {
    return (
      <div className="business-ideas">
        <div className="page-header">
          <p>Бизнес-идеи</p>
        </div>

        <div className="ideas">
          <BusinessIdea
            name="Совмещенный мерчандайзинг"
            img={img1} />
          <BusinessIdea
            name="Цифровизация, искусственный интеллект"
            img={img2} />
          <BusinessIdea
            name="Дистрибьюция"
            img={img3} />
          <BusinessIdea
            name="Диджитал"
            img={img4} />
          <BusinessIdea
            name="Закупки и импорт"
            img={img5} />
          <BusinessIdea
            name="Экспорт"
            img={img6} />
          <BusinessIdea
            name="Инфраструктура"
            img={img7} />
          <BusinessIdea
            name="Импортозамещение"
            img={img8} />
          <BusinessIdea
            name="Обучение кадров"
            img={img9} />
        </div>

        <div className="page-header">
          <p>Потенциальные партнеры</p>
        </div>

        <div className="reviews">
          <Review
            img={reviewImg1}
            name="Морозко"
            rating="3,7"
            reviews="12"
            text="Производственная компания Морозко – ведущий изготовитель замороженной продукции. Известные торговы..." />
          <Review
            img={reviewImg2}
            name="Нижегородский масложировой комбинат"
            rating="3,1"
            reviews="2"
            text="Группа компаний «НМЖК» — производитель майонеза «Ряба», соусов «Астория», семечек «Степановна», туалетн..." />
          <Review
            img={reviewImg3}
            name="Айсберри"
            rating="3,7"
            reviews="3"
            text="Айсберри — №1 на рынке мороженого Москвы и Московской области, входит в тройку лидеров российского  ..." />
        </div>

        <FindBox />
      </div>
    );
  }
}

export default BusinessIdeas;