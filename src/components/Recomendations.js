import '../assets/css/Recomendations.css';
import React from 'react';
import book1 from '../assets/images/recomendations/books/1.png';
import book2 from '../assets/images/recomendations/books/2.png';
import book3 from '../assets/images/recomendations/books/3.png';
import book4 from '../assets/images/recomendations/books/4.png';
import FindBox from './FindBox';

class Recomendations extends React.Component {
  render() {
    return (
      <div className="recomendations">
        <div className="page-header">
          <p>рекомендации</p>
        </div>

        <div className="books">
          <p className="header">Подборка лучшей художественной и бизнес-литературы по рекомендации наших экспертов</p>

          <div className="list">
            <div className="book">
              <div className="img">
                <img src={book1} alt="" />
              </div>
              
              <div className="info">
                <p className="name">Батлер – Боудон Том, «50 Великих книг о бизнесе»</p>

                <p className="description">Вдохновляющие идеи и гениальные инструменты из 50 самых блестящих бизнес-книг за всю историю…</p>
              </div>
            </div>
            <div className="book">
              <div className="img">
                <img src={book2} alt="" />
              </div>
              
              <div className="info">
                <p className="name">Лумис Кэрол,Уоррен Баффетт «Танцуя к богатству»</p>

                <p className="description">Биография всемирно известного инвестора в формате эссе, статей и историй из жизни. Уоррен Баффетт…</p>
              </div>
            </div>
            <div className="book">
              <div className="img">
                <img src={book3} alt="" />
              </div>
              
              <div className="info">
                <p className="name">Франклин Бенджамин. «Путь к богатству. Автобиография»</p>

                <p className="description">Эта книга устраняет чудовищную историческую несправедливость: она впервые представляет отечестве…</p>
              </div>
            </div>
            <div className="book">
              <div className="img">
                <img src={book4} alt="" />
              </div>
              
              <div className="info">
                <p className="name">Минцберг Генри. «Менеджмент: природа и структура организации». </p>

                <p className="description">Генри Минцберг, признанный гуру теории и практики управления…</p>
              </div>
            </div>
          </div>
        </div>

        <FindBox />
      </div>
    );
  }
}

export default Recomendations;