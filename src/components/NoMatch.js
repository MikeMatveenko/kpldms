import React from 'react';
import '../assets/css/NoMatch.css';

class Menu extends React.Component {
  render() {
    return (
      <div className="no-match">
        <p className="header">404</p>

        <p>Страница не найдена.</p>
      </div>
    );
  }
}

export default Menu;