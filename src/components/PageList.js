import React from 'react';
import '../assets/css/PageList.css';

class PageList extends React.Component {
  constructor (props) {
    super(props)

    this.pageListRef = React.createRef()

    this.scroll = this.scroll.bind(this)
  }

  componentDidMount () {
    document.querySelector('#root > :nth-child(2)').onscroll = this.scroll
  }

  scroll (e) {
    let as = Array.from(this.pageListRef.current.children).reverse();

    for (let i = 0; i < as.length; i++) {
      let a = as[i]

      let menu = document.querySelector('#root > .menu').offsetHeight
      let block = document.querySelector(a.getAttribute('href'))

      if (block.getBoundingClientRect().top - menu < 0) {
        Array.from(a.parentNode.children).forEach(a_ => {
          a_.classList.remove('active')
        })

        a.classList.add('active')

        break;
      }
    }
  }

  render () {
    return (
      <div className="page-links" ref={this.pageListRef}>
        {this.props.children}
      </div>
    );
  }
}

class PageLink extends React.Component {
  click (e) {
    e.preventDefault()

    let menu = document.querySelector('#root > .menu').offsetHeight// высота меню
    let el = e.target.getAttribute('href')

    document.querySelector('#root > :nth-child(2)').scrollTop = document.querySelector(el).offsetTop + 130 - (menu > 100 ? 40 : 0)

    Array.from(e.target.parentNode.children).forEach(a => {
      a.classList.remove('active')
    });

    e.target.classList.add('active')
  }
  
  render () {
    return (
      <a onClick={this.click} className={this.props.active ? "active" : ""} href={this.props.href}>{this.props.label}</a>
    );
  }
}

export {
  PageList,
  PageLink
};