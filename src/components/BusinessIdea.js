import React from 'react';
import '../assets/css/BusinessIdea.css';
import { useHistory } from 'react-router-dom';

function BusinessIdea (props) {
  let history = useHistory();

  function handleClick() {
    history.push("/business-ideas/ideas/1");
  }

  return (
    <div className="business-idea" onClick={handleClick}>
      <img src={props.img} alt="" />

      <div></div>

      <p>{props.name}</p>
    </div>
  );
}

export default BusinessIdea;