import '../assets/css/Contacts.css';
import React from 'react';
import FindBox from './FindBox';
import map from '../assets/images/contacts/map.png';

class Contacts extends React.Component {
  render() {
    return (
      <div className="contacts">
        <div className="page-header">
          <p>контакты</p>
        </div>

        <div className="list">
          <div className="map">
            <img src={map} alt="" />
          </div>

          <div className="data">
            <div className="address">
              <p className="header">Адрес</p>

              <p>119002, Москва, ул. Арбат, д.35, офис 635</p>
            </div>

            <p className="header">Координаторы Клуба</p>

            <div className="coordinator">
              <p className="name">Иван Добросоцкий</p>

              <div className="connect">
                <p>mob:</p>

                <p>+7-926-880-07-26</p>
              </div>

              <div className="connect">
                <p>email:</p>

                <p>fognini@inbox.ru</p>
              </div>
            </div>

            <div className="coordinator">
              <p className="name">Ирина Лишневская</p>

              <div className="connect">
                <p>mob:</p>

                <p>+7-916-682-88-53</p>
              </div>

              <div className="connect">
                <p>email:</p>

                <p>ilishnevskaya@gmail.com</p>
              </div>
            </div>
          </div>
        </div>

        <div className="page-header">
          <p>обратная связь</p>
        </div>

        <div className="form">
          <textarea placeholder="Введите текст" />

          <div className="submit">
            <div className="polit">
              <input type="checkbox" id="polit" />
              <label for="polit"></label>

              <label>Согласен с политикой обработки персональных данных</label>
            </div>

            <button className="send">Отправить</button>
          </div>
        </div>

        <FindBox />
      </div>
    );
  }
}

export default Contacts;