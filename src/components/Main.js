import React from 'react';
import '../assets/css/Main.css';
import link1 from '../assets/images/main/links/1.png';
import linkHover1 from '../assets/images/main/links/1-hover.png';
import link2 from '../assets/images/main/links/2.png';
import linkHover2 from '../assets/images/main/links/2-hover.png';
import link3 from '../assets/images/main/links/3.png';
import linkHover3 from '../assets/images/main/links/3-hover.png';
import link4 from '../assets/images/main/links/4.png';
import linkHover4 from '../assets/images/main/links/4-hover.png';
import link5 from '../assets/images/main/links/5.png';
import linkHover5 from '../assets/images/main/links/5-hover.png';
import link6 from '../assets/images/main/links/6.png';
import linkHover6 from '../assets/images/main/links/6-hover.png';
import link7 from '../assets/images/main/links/7.png';
import linkHover7 from '../assets/images/main/links/7-hover.png';
import link8 from '../assets/images/main/links/8.png';
import linkHover8 from '../assets/images/main/links/8-hover.png';
import link9 from '../assets/images/main/links/9.png';
import linkHover9 from '../assets/images/main/links/9-hover.png';
import link10 from '../assets/images/main/links/10.png';
import linkHover10 from '../assets/images/main/links/10-hover.png';
import link11 from '../assets/images/main/links/11.png';
import linkHover11 from '../assets/images/main/links/11-hover.png';
import FindBox from './FindBox';

class Menu extends React.Component {
  constructor (props) {
    super(props)

    this.link1 = React.createRef();
    this.link2 = React.createRef();
    this.link3 = React.createRef();
    this.link4 = React.createRef();
    this.link5 = React.createRef();
    this.link6 = React.createRef();
    this.link7 = React.createRef();
    this.link8 = React.createRef();
    this.link9 = React.createRef();
    this.link10 = React.createRef();
    this.link11 = React.createRef();
  }

  render() {
    return (
      <div className="main">
        <div className="info">
          <FindBox black />

          <p className="tagline">Созидай, радуясь жизни</p>

          <div className="text-info">
            <p className="header">О клубе</p>

            <p className="text">Клуб «Первых лиц» — современные практики потребительского рынка. Клуб создан по аналогу существующего немецкого делового Клуба, учрежденного в Германии в 1962 году  (www.mmm-club.de).<br />Основанный в 2019 году закрытый Клуб «Первых лиц» поддерживает обмен опытом, касающимся современных методов торговли и продаж с целью прогрессивного обеспечения потребителей. Он занимается новыми разработками в сфере потребительских товаров. Клуб проводит конгрессы, семинары и учебно-ознакомительные поездки, а также поддерживает профессиональные связи. В работе клуба могут принимать участие все предприниматели и предприятия торговли, промышленности и других отраслей экономики.</p>
          </div>
        </div>

        <div className="links">
          <div className="page-header">
            <p>Компании</p>
          </div>

          <div className="table">
            <a href="https://www.auchan.ru/" target="_blank" onMouseOver={ () => { this.link1.current.setAttribute('src', linkHover1) } } onMouseLeave={ () => { this.link1.current.setAttribute('src', link1) } }><img ref={ this.link1 } src={link1} /></a>
            <a href="https://www.nmgk.ru/" target="_blank" onMouseOver={ () => { this.link2.current.setAttribute('src', linkHover2) } } onMouseLeave={ () => { this.link2.current.setAttribute('src', link2) } }><img ref={ this.link2 } src={link2} /></a>
            <a href="https://pkm-group.ru/brands/morozko/" target="_blank" onMouseOver={ () => { this.link3.current.setAttribute('src', linkHover3) } } onMouseLeave={ () => { this.link3.current.setAttribute('src', link3) } }><img ref={ this.link3 } src={link3} /></a>
            <a href="http://www.belaya-dacha.ru/" target="_blank" onMouseOver={ () => { this.link4.current.setAttribute('src', linkHover4) } } onMouseLeave={ () => { this.link4.current.setAttribute('src', link4) } }><img ref={ this.link4 } src={link4} /></a>
            <a href="https://www.ekoniva-apk.ru/" target="_blank" onMouseOver={ () => { this.link5.current.setAttribute('src', linkHover5) } } onMouseLeave={ () => { this.link5.current.setAttribute('src', link5) } }><img ref={ this.link5 } src={link5} /></a>
            <a href="http://www.kabosh.ru/" target="_blank" onMouseOver={ () => { this.link6.current.setAttribute('src', linkHover6) } } onMouseLeave={ () => { this.link6.current.setAttribute('src', link6) } }><img ref={ this.link6 } src={link6} /></a>
            <a href="http://www.orimi.com/" target="_blank" onMouseOver={ () => { this.link7.current.setAttribute('src', linkHover7) } } onMouseLeave={ () => { this.link7.current.setAttribute('src', link7) } }><img ref={ this.link7 } src={link7} /></a>
            <a href="https://www.iceberry.ru/" target="_blank" onMouseOver={ () => { this.link8.current.setAttribute('src', linkHover8) } } onMouseLeave={ () => { this.link8.current.setAttribute('src', link8) } }><img ref={ this.link8 } src={link8} /></a>
            <a href="https://slavjankamarket.ru/" target="_blank" onMouseOver={ () => { this.link9.current.setAttribute('src', linkHover9) } } onMouseLeave={ () => { this.link9.current.setAttribute('src', link9) } }><img ref={ this.link9 } src={link9} /></a>
            <a href="http://grainholding.ru/" target="_blank" onMouseOver={ () => { this.link10.current.setAttribute('src', linkHover10) } } onMouseLeave={ () => { this.link10.current.setAttribute('src', link10) } }><img ref={ this.link10 } src={link10} /></a>
            <a href="https://smart174.ru/" target="_blank" onMouseOver={ () => { this.link11.current.setAttribute('src', linkHover11) } } onMouseLeave={ () => { this.link11.current.setAttribute('src', link11) } }><img ref={ this.link11 } src={link11} /></a>
          </div>
        </div>
      </div>
    );
  }
}

export default Menu;