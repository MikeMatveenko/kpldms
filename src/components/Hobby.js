import '../assets/css/Hobby.css';
import React from 'react';
import AccountCard from './AccountCard';
import FindBox from './FindBox';
import img1 from '../assets/images/hobby/1.png';
import img2 from '../assets/images/hobby/2.png';
import img3 from '../assets/images/hobby/3.png';
import img4 from '../assets/images/hobby/4.png';

class Hobby extends React.Component {
  render() {
    return (
      <div className="hobby">
        <div className="page-header">
          <p>хобби</p>
        </div>

        <div className="one-hobby">
          <AccountCard
            img={img3}
            name="Лола Тимофей"
            job="Студент второго курса"
            position="Я студент второго курса, изучаю биологию в Соединённых Штатах Америки. В будущем хочу заниматься проектами в области нейробиологических исследований человечного мозга. На данный момент занимаюсь разработкой стартапа в области онлайн обучения. 

            Люблю читать научные работы и исследования. Так же я увлекаюсь спортом, в частности игрой в большой теннис. Люблю отечественное кино. Изучаю французский язык."
            social={[
              {
                type: 'instagram',
                href: "https://instagram.com"
              },
              {
                type: 'facebook',
                href: 'https://facebook.com'
              },
              {
                type: 'twitter',
                href: 'https://twitter.com'
              }
            ]} />
        </div>
        <div className="container">
          <div className="characters">
            <a href="#А">А</a>
            <a href="#Б">Б</a>
            <a href="#В">В</a>
            <a href="#Д">Д</a>
            <a href="#Е">Е</a>
            <a href="#Ё">Ё</a>
            <a href="#Ж">Ж</a>
            <a href="#З">З</a>
            <a href="#И">И</a>
            <a href="#К">К</a>
            <a href="#Л">Л</a>
            <a href="#М">М</a>
            <a href="#Н">Н</a>
            <a href="#О">О</a>
            <a href="#П">П</a>
            <a href="#Р">Р</a>
            <a href="#С">С</a>
            <a href="#Т">Т</a>
            <a href="#У">У</a>
            <a href="#Ф">Ф</a>
            <a href="#Х">Х</a>
            <a href="#Ц">Ц</a>
            <a href="#Ч">Ч</a>
            <a href="#Щ">Щ</a>
            <a href="#Ш">Ш</a>
            <a href="#Ъ">Ъ</a>
            <a href="#Ы">Ы</a>
            <a href="#Ь">Ь</a>
            <a href="#Э">Э</a>
            <a href="#Ю">Ю</a>
            <a href="#Я">Я</a>
          </div>

          <div className="list">
            <AccountCard
              id="Д"
              img={img1}
              name="Деменов Борис"
              job="Руководитель отдела продаж"
              position="В свободное от работы время увлекаюсь охотой и рыбалкой, слежу за смешанными единоборствами и футболом, а по вечерам люблю читать деловые книги или смотреть документальные фильмы и сериалы."
              social={[
                {
                  type: 'instagram',
                  href: "https://instagram.com"
                },
                {
                  type: 'facebook',
                  href: 'https://facebook.com'
                },
                {
                  type: 'twitter',
                  href: 'https://twitter.com'
                }
              ]} />
            <AccountCard
              img={img2}
              name="Добросоцкая Дарья"
              job="Студентка МГИМО МИД РФ"
              position="Увлекаюсь игрой на фортепьяно, нравится изучать иностранные языки, люблю путешествовать"
              social={[
                {
                  type: 'instagram',
                  href: "https://instagram.com"
                },
                {
                  type: 'facebook',
                  href: 'https://facebook.com'
                },
                {
                  type: 'twitter',
                  href: 'https://twitter.com'
                }
              ]} />
            <AccountCard
              img={img3}
              name="Лола Тимофей"
              job="Студент второго курса"
              position="Я студент второго курса, изучаю биологию в Соединённых Штатах Америки. В будущем хочу заниматься проектами в области нейробиологических исследований человечного мозга. На данный момент занимаюсь разработкой стартапа в области онлайн обучения. 

              Люблю читать научные работы и исследования. Так же я увлекаюсь спортом, в частности игрой в большой теннис. Люблю отечественное кино. Изучаю французский язык."
              social={[
                {
                  type: 'instagram',
                  href: "https://instagram.com"
                },
                {
                  type: 'facebook',
                  href: 'https://facebook.com'
                },
                {
                  type: 'twitter',
                  href: 'https://twitter.com'
                }
              ]} />
            <AccountCard
              img={img4}
              name="Петрякова Наталья"
              job="Координатор Клуба «Первых Лиц»"
              position="Занимаюсь спецпроектами Клуба, коммуникациями, организацией вебинаров и выездных заседаний Клуба.

              Интересуюсь футболом. Играю в русский бильярд. Люблю ходить по театрам."
              social={[
                {
                  type: 'instagram',
                  href: "https://instagram.com"
                },
                {
                  type: 'facebook',
                  href: 'https://facebook.com'
                },
                {
                  type: 'twitter',
                  href: 'https://twitter.com'
                }
              ]} />
          </div>
        </div>

        <FindBox />
      </div>
    );
  }
}

export default Hobby;